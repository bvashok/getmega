import React from "react";
import { Route, Redirect } from "react-router-dom";
import routeNames from "./routeNames";
import Cookies from "js-cookie";

const AnonymousRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (Cookies.get("Auth_token") === undefined) {
        return <Component {...props} />;
      } else {
        return <Redirect to={routeNames.dashboard} />;
      }
    }}
  />
);

export default AnonymousRoute;
