import React from "react";
import { Route, Redirect } from "react-router-dom";
import routeNames from "./routeNames";
import Cookies from "js-cookie";

const AuthenticatedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      Cookies.get("Auth_token") ? (
        <Component {...props} />
      ) : (
        <Redirect to={routeNames.login} />
      )
    }
  />
);

export default AuthenticatedRoute;
