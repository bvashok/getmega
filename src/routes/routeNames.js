const routeNames = {
  dashboard: "/dashboard",
  home: "/",
  login: "/login",
};

export default routeNames;
