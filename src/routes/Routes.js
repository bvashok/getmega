import React from "react";
import { Route, Switch } from "react-router-dom";
import { Dashboard, Home, NotFound, Login } from "../components";
import { AnonymousRoute, AuthenticatedRoute } from "./";

const Routes = () => {
  return (
    <div>
      <Switch>
        <AnonymousRoute exact path="/" component={Home} />
        <AnonymousRoute exact path="/login" component={Login} />
        <AuthenticatedRoute exact path="/dashboard" component={Dashboard} />
        <Route default component={NotFound} />
      </Switch>
    </div>
  );
};

export default Routes;
