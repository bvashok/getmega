import React, { useState, createContext } from "react";

export const CompaniesUpdateContext = createContext({
  Companies: [],
  setCompanies: () => {},
});

const CompaniesUpdateProvider = (props) => {
  const [companies, setCompanies] = useState([]);

  return (
    <CompaniesUpdateContext.Provider value={{ companies, setCompanies }}>
      {props.children}
    </CompaniesUpdateContext.Provider>
  );
};

export default CompaniesUpdateProvider;
