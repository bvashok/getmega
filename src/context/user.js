import React, { useState, createContext, useEffect } from "react";

export const UserUpdateContext = createContext({
  user: {},
  setUser: () => {},
});

const UserUpdateProvider = (props) => {
  const [user, setUser] = useState({});

  useEffect(() => {
    (async () => {
      if (process.browser) {
        if (localStorage.getItem("userDetails")) {
          let userDetails = JSON.parse(localStorage.getItem("userDetails"));
          setUser(userDetails);
        } else {
          setUser({});
        }
      }
    })();
  }, []);

  return (
    <UserUpdateContext.Provider value={{ user, setUser }}>
      {props.children}
    </UserUpdateContext.Provider>
  );
};

export default UserUpdateProvider;
