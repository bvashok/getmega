// import styled from "styled-components";
import React from "react";
import CanvasJSReact from "../assets/canvasjs.react";
import { withStyles } from "@material-ui/core/styles";
import { useState, useEffect } from "react";
import PropTypes from "prop-types";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const styles = () => ({
  menuActive: {
    color: "red",
    borderBottom: "1px solid red",
    cursor: "pointer",
  },
  menuInactive: {
    color: "blue",
    borderBottom: "1px solid blue",
    cursor: "pointer",
  },
});

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

function CompanyMonthlyRevenue(props) {
  const { companies, classes } = props;
  const [activeCompany, setActiveCompany] = useState({});

  useEffect(() => {
    if (companies && companies.length > 0) {
      if (process.browser) {
        if (localStorage.getItem("activeCompany")) {
          setActiveCompany(JSON.parse(localStorage.getItem("activeCompany")));
        } else {
          setActiveCompany(companies[0]);
        }
      }
    }
  }, [companies]);

  const onCompanyClick = (item) => {
    setActiveCompany(item);
    localStorage.setItem("activeCompany", JSON.stringify(item));
  };

  let dataPoints = [];
  if (activeCompany && Object.keys(activeCompany).length > 0) {
    Object.keys(activeCompany).forEach((item) => {
      if (months.includes(item)) {
        let dataPoint = {
          label: item,
          y: parseFloat(activeCompany[item].toString().replace(/,/g, "")),
        };
        dataPoints.push(dataPoint);
      }
    });
  }

  const options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: "light2", //"light1", "dark1", "dark2"
    axisY: {
      includeZero: true,
    },
    data: [
      {
        type: "column", //change type to bar, line, area, pie, etc
        //indexLabel: "{y}", //Shows y value on all Data Points
        indexLabelFontColor: "#5A5757",
        indexLabelPlacement: "outside",
        dataPoints: dataPoints,
      },
    ],
  };
  return (
    <div style={{ padding: "20px 50px" }}>
      <h3 style={{ textAlign: "center" }}>Company Monthly Revenue</h3>
      <div style={{ display: "flex", justifyContent: "center" }}>
        {companies &&
          companies.length > 0 &&
          companies.map((item) => {
            return (
              <div
                style={{
                  margin: "10px",
                }}
                className={
                  item.Company === activeCompany.Company
                    ? classes.menuActive
                    : classes.menuInactive
                }
                onClick={() => {
                  onCompanyClick(item);
                }}
              >
                {item.Company}
              </div>
            );
          })}
      </div>
      <div style={{ marginTop: "20px" }}>
        <CanvasJSChart options={options} />
      </div>
    </div>
  );
}

CompanyMonthlyRevenue.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CompanyMonthlyRevenue);
