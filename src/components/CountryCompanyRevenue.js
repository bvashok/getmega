// import styled from "styled-components";
import React from "react";
import CanvasJSReact from "../assets/canvasjs.react";
import { withStyles } from "@material-ui/core/styles";
import { useState, useEffect } from "react";
import PropTypes from "prop-types";
// var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const styles = (theme) => ({
  menuActive: {
    color: "red",
    borderBottom: "1px solid red",
    cursor: "pointer",
  },
  menuInactive: {
    color: "blue",
    borderBottom: "1px solid blue",
    cursor: "pointer",
  },
});

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

function CountryCompanyRevenue(props) {
  const { companies, classes } = props;
  const [activeCountry, setActiveCountry] = useState({});
  const [countryCompanies, setCountryComapnies] = useState([]);

  //Calculating comapnies total revenue over the year
  useEffect(() => {
    let companiesTotalRevenueList;
    let countriesList;
    let countryCompaniesList;
    if (companies && companies.length > 0) {
      companiesTotalRevenueList = [];
      countriesList = [];
      countryCompaniesList = [];
      companies.forEach((company) => {
        let totalRevenue = 0;
        Object.keys(company).forEach((item) => {
          if (months.includes(item)) {
            totalRevenue =
              totalRevenue +
              parseFloat(company[item].toString().replace(/,/g, ""));
          }
        });
        let obj = {
          name: company.Company,
          totalRevenue: totalRevenue,
          country: company.Country,
        };
        companiesTotalRevenueList.push(obj);
        if (!countriesList.includes(company.Country)) {
          countriesList.push(company.Country);
        }
      });
      if (countriesList.length > 0) {
        countriesList.forEach((item) => {
          let obj = {
            id: item,
          };
          countryCompaniesList.push(obj);
        });
      }
      if (companiesTotalRevenueList.length > 0) {
        companiesTotalRevenueList.forEach((item) => {
          let index = countryCompaniesList.findIndex(
            (element) => element.id === item.country
          );
          if (index > -1) {
            countryCompaniesList[index][item.name] = item.totalRevenue;
          }
        });
        console.log("countryCompanies", countryCompaniesList);
        setCountryComapnies(countryCompaniesList);
        if (process.browser) {
          if (localStorage.getItem("activeCountry")) {
            setActiveCountry(JSON.parse(localStorage.getItem("activeCountry")));
          } else {
            setActiveCountry(countryCompaniesList[0]);
          }
        }
      }
    }
  }, [companies]);

  const onCountryClick = (item) => {
    setActiveCountry(item);
    localStorage.setItem("activeCountry", JSON.stringify(item));
  };

  //classifying data country wise

  let dataPoints = [];
  if (activeCountry && Object.keys(activeCountry).length > 0) {
    Object.keys(activeCountry).forEach((item) => {
      if (item !== "id") {
        let dataPoint = {
          label: item,
          y: parseFloat(activeCountry[item].toString().replace(/,/g, "")),
        };
        dataPoints.push(dataPoint);
      }
    });
    console.log("dataPoints", dataPoints);
  }

  let pieDataPoints = [];
  if (activeCountry && Object.keys(activeCountry).length > 0) {
    let countryRevenue = 0;
    Object.keys(activeCountry).forEach((item) => {
      if (item !== "id") {
        countryRevenue =
          countryRevenue +
          parseFloat(activeCountry[item].toString().replace(/,/g, ""));
      }
    });
    console.log("countryRevenue", countryRevenue);
    Object.keys(activeCountry).forEach((item) => {
      if (item !== "id") {
        let dataPoint = {
          label: item,
          y: (
            (parseFloat(activeCountry[item].toString().replace(/,/g, "")) *
              100) /
            countryRevenue
          ).toFixed(2),
          amount: activeCountry[item],
        };
        pieDataPoints.push(dataPoint);
      }
    });
    console.log("pieDataPoints", pieDataPoints);
  }

  const pieOptions = {
    exportEnabled: true,
    animationEnabled: true,
    data: [
      {
        type: "pie",
        startAngle: 75,
        toolTipContent: "<b>{label}</b>: {y}% ({amount})",
        showInLegend: "true",
        legendText: "{label}",
        indexLabelFontSize: 16,
        indexLabel: "{label} - {y}% ({amount})",
        dataPoints: pieDataPoints,
      },
    ],
  };

  return (
    <div style={{ padding: "20px 50px" }}>
      <h3 style={{ textAlign: "center" }}>Country Companies Revenue</h3>
      <div style={{ display: "flex", justifyContent: "center" }}>
        {countryCompanies &&
          countryCompanies.length > 0 &&
          countryCompanies.map((item) => {
            return (
              <div
                style={{
                  margin: "10px",
                }}
                className={
                  item.id === activeCountry.id
                    ? classes.menuActive
                    : classes.menuInactive
                }
                onClick={() => onCountryClick(item)}
              >
                {item.id}
              </div>
            );
          })}
      </div>
      <div style={{ marginTop: "20px" }}>
        <CanvasJSChart options={pieOptions} />
      </div>
    </div>
  );
}

CountryCompanyRevenue.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CountryCompanyRevenue);
