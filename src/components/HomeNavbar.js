// import styled from "styled-components";
import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "@material-ui/core";
import routeNames from "../routes/routeNames";

function HomeNavbar() {
  let history = useHistory();
  return (
    <div style={{ display: "flex", background: "#20232a", padding: "15px" }}>
      <div style={{ margin: "auto auto auto 10px" }}>
        <a href="/" style={{ color: "white", textDecoration: "none" }}>
          HOME
        </a>
      </div>
      <div style={{ margin: "auto 10px auto auto" }}>
        <Button
          color="primary"
          variant="contained"
          onClick={() => {
            history.push(routeNames.login);
          }}
        >
          Login
        </Button>
      </div>
    </div>
  );
}

export default HomeNavbar;
