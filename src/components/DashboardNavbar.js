import styled from "styled-components";
import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import { useState, useRef, useEffect, useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { UserUpdateContext } from "../context/user";
import Cookies from "js-cookie";
import CircularProgress from "@material-ui/core/CircularProgress";
import fire from "../config/fire";
import routeNames from "../routes/routeNames";

const LogoutPaper = styled.div`
  padding: 25px 20px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  line-height: 40px;
  text-align: center;
`;

const AvatarStyle = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 10px;
`;

const styles = (theme) => ({
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
});

function DashboardNavbar(props) {
  const { classes } = props;
  const [loader, setLoader] = useState(false);
  const { user, setUser } = useContext(UserUpdateContext);
  let history = useHistory();
  const [open, setOpen] = useState(false);

  const anchorRef = useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  const onLogout = () => {
    setLoader(true);
    setUser({});
    fire.analytics().logEvent("logout");
    localStorage.clear();
    Cookies.remove("Auth_token");
    history.push(routeNames.login);
    setLoader(false);
  };

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <>
      {loader && (
        <div className="loader">
          <CircularProgress color="primary" />
          <div className="text">Logging out</div>
        </div>
      )}
      <div style={{ display: "flex", background: "#1976d2" }}>
        <div style={{ margin: "auto auto auto 10px", color: "white" }}>
          DASHBOARD
        </div>
        <div style={{ margin: "auto 10px auto auto" }}>
          <IconButton
            ref={anchorRef}
            aria-controls={open ? "menu-list-grow" : undefined}
            aria-haspopup="true"
            onClick={handleToggle}
          >
            <Avatar
              alt={user.name}
              src={user.avatar}
              style={{ textTransform: "uppercase" }}
            />
          </IconButton>
        </div>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: placement === "bottom end",
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <LogoutPaper
                    autoFocusItem={open}
                    onKeyDown={handleListKeyDown}
                  >
                    <AvatarStyle>
                      <Avatar
                        alt={user.name}
                        src={user.avatar}
                        className={classes.large}
                      />
                    </AvatarStyle>
                    <div
                      style={{
                        fontWeight: "bold",
                        color: "#555555",
                      }}
                    >
                      {user.name}
                    </div>
                    <div
                      style={{
                        color: "#555555",
                        marginBottom: "10px",
                      }}
                    >
                      {user.email}
                    </div>
                    <div>
                      <Button
                        style={{ border: "1px solid #555555" }}
                        onClick={() => onLogout()}
                      >
                        Logout
                      </Button>
                    </div>
                  </LogoutPaper>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    </>
  );
}

DashboardNavbar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DashboardNavbar);
