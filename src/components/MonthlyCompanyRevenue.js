// import styled from "styled-components";
import React from "react";
import CanvasJSReact from "../assets/canvasjs.react";
import { withStyles } from "@material-ui/core/styles";
import { useState, useEffect } from "react";
import PropTypes from "prop-types";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const styles = (theme) => ({
  menuActive: {
    color: "red",
    borderBottom: "1px solid red",
    cursor: "pointer",
  },
  menuInactive: {
    color: "blue",
    borderBottom: "1px solid blue",
    cursor: "pointer",
  },
});

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

function MonthlyCompanyRevenue(props) {
  const { companies, classes } = props;
  const [activeMonth, setActiveMonth] = useState({});
  const [revenueMonthsList, setRevenueMonthsList] = useState("");

  let dataPoints = [];
  useEffect(() => {
    let monthsList = [];
    if (companies && companies.length > 0) {
      months.forEach((item) => {
        let obj = {
          id: item,
        };
        monthsList.push(obj);
      });
      companies.forEach((company) => {
        if (monthsList.length > 0) {
          monthsList.forEach((item) => {
            item[company.Company] = company[item.id];
          });
        }
      });
      setRevenueMonthsList(monthsList);
      if (process.browser) {
        if (localStorage.getItem("activeMonth")) {
          setActiveMonth(JSON.parse(localStorage.getItem("activeMonth")));
        } else {
          setActiveMonth(monthsList[0]);
        }
      }
    }
  }, [companies]);

  const onMonthClick = (item) => {
    setActiveMonth(item);
    localStorage.setItem("activeMonth", JSON.stringify(item));
  };

  if (activeMonth && Object.keys(activeMonth).length > 0) {
    Object.keys(activeMonth).forEach((item) => {
      if (item !== "id") {
        let dataPoint = {
          label: item,
          y: parseFloat(activeMonth[item].toString().replace(/,/g, "")),
        };
        dataPoints.push(dataPoint);
      }
    });
    console.log("dataPoints", dataPoints);
  }

  const options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: "light2", //"light1", "dark1", "dark2"
    axisY: {
      includeZero: true,
    },
    data: [
      {
        type: "column", //change type to bar, line, area, pie, etc
        //indexLabel: "{y}", //Shows y value on all Data Points
        indexLabelFontColor: "#5A5757",
        indexLabelPlacement: "outside",
        dataPoints: dataPoints,
      },
    ],
  };
  return (
    <div style={{ padding: "20px 50px" }}>
      <h3 style={{ textAlign: "center" }}>Monthly Companies Revenue</h3>
      <div style={{ display: "flex", justifyContent: "center" }}>
        {revenueMonthsList &&
          revenueMonthsList.length > 0 &&
          revenueMonthsList.map((item) => {
            return (
              <div
                style={{
                  margin: "10px",
                }}
                className={
                  item.id === activeMonth.id
                    ? classes.menuActive
                    : classes.menuInactive
                }
                onClick={() => onMonthClick(item)}
              >
                {item.id}
              </div>
            );
          })}
      </div>
      <div style={{ marginTop: "20px" }}>
        <CanvasJSChart options={options} />
      </div>
    </div>
  );
}

MonthlyCompanyRevenue.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MonthlyCompanyRevenue);
