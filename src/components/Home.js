// import styled from "styled-components";
import React from "react";
import { HomeNavbar } from "./index";

function Home() {
  return (
    <>
      <HomeNavbar></HomeNavbar>
      <div className="body_center">
        <div style={{ fontSize: "4rem", color: "#514F4F" }}>
          Welcome to Revenue Report App
        </div>
      </div>
    </>
  );
}

export default Home;
