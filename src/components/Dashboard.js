import styled from "styled-components";
import React from "react";
import {
  DashboardNavbar,
  CompanyMonthlyRevenue,
  CountryCompanyRevenue,
  MonthlyCompanyRevenue,
} from "./index";
import fire from "../config/fire";
import { useState, useEffect, useContext } from "react";
import { CompaniesUpdateContext } from "../context/companies";
import CircularProgress from "@material-ui/core/CircularProgress";
import Paper from "@material-ui/core/Paper";
import { UserUpdateContext } from "../context/user";

const WelcomeMessage = styled.div`
  padding: 20px 10px;
  font-size: 24px;
  color: grey;
  text-align: center;
`;

function Dashboard() {
  const { companies, setCompanies } = useContext(CompaniesUpdateContext);
  const [loader, setLoader] = useState(false);
  const { user } = useContext(UserUpdateContext);

  useEffect(() => {
    const db = fire.firestore();
    setLoader(true);
    db.collection("companies")
      .get()
      .then(function (querySnapshot) {
        let fetchedCompanies = [];
        querySnapshot.forEach(function (doc) {
          // doc.data() is never undefined for query doc snapshots
          fetchedCompanies.push(doc.data());
        });
        setCompanies(fetchedCompanies);
        setLoader(false);
      })
      .catch((error) => {
        console.log("error", error);
        setLoader(false);
      });
  }, []);

  return (
    <>
      {loader && (
        <div className="loader">
          <CircularProgress color="primary" />
        </div>
      )}
      <DashboardNavbar></DashboardNavbar>
      <WelcomeMessage>
        Hi, <span style={{ fontWeight: "bold" }}>{user.name} </span>{" "}
        &#128578;... Welcome to Revenue Report Dashboard.
      </WelcomeMessage>
      {companies && companies.length > 0 && (
        <div>
          <Paper elevation={3} style={{ margin: "10px" }}>
            <CompanyMonthlyRevenue
              companies={companies}
            ></CompanyMonthlyRevenue>
          </Paper>
          <Paper elevation={3} style={{ margin: "0px 10px" }}>
            <CountryCompanyRevenue
              companies={companies}
            ></CountryCompanyRevenue>
          </Paper>
          <Paper elevation={3} style={{ margin: "10px" }}>
            <MonthlyCompanyRevenue
              companies={companies}
            ></MonthlyCompanyRevenue>
          </Paper>
        </div>
      )}
    </>
  );
}

export default Dashboard;
