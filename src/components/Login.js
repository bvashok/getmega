import React from "react";
import fire from "../config/fire";
import firebase from "firebase";
import { useHistory } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import { useContext } from "react";
import { UserUpdateContext } from "../context/user";
import Cookies from "js-cookie";

function Login() {
  let history = useHistory();
  const { setUser } = useContext(UserUpdateContext);
  const onGoogleLogin = () => {
    var provider = new firebase.auth.GoogleAuthProvider();
    fire
      .auth()
      .signInWithPopup(provider)
      .then(function (result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        Cookies.set("Auth_token", token);
        var user = result.user;
        const userDetails = {
          name: user.displayName,
          email: user.email,
          phone: user.phoneNumber,
          avatar: user.photoURL,
          authenticated: true,
        };
        fire.analytics().logEvent("login");
        setUser(userDetails);
        localStorage.setItem("userDetails", JSON.stringify(userDetails));
        history.push("/dashboard");
        // ...
      })
      .catch(function (error) {
        console.log("error", error);
      });
  };

  return (
    <>
      <div style={{ display: "flex", padding: "10px" }}>
        <a style={{ margin: "auto auto auto 10px" }} href="/">
          HOME
        </a>
      </div>
      <div className="body_center">
        <Paper
          elevation={3}
          style={{
            padding: "50px 20px",
            textAlign: "center",
            minWidth: "300px",
            width: "30%",
          }}
        >
          <h2>Login</h2>
          <div
            onClick={() => onGoogleLogin()}
            style={{ cursor: "pointer", color: "blue" }}
          >
            <img
              src="btn_google_signin.png"
              alt="google login"
              width="200px"
            ></img>
          </div>
        </Paper>
      </div>
    </>
  );
}

export default Login;
