// import styled from "styled-components";
import React from "react";

function NotFound() {
  return (
    <div className="body_center">
      <div style={{ fontSize: "4rem", color: "#514F4F" }}>Page Not Found</div>
      <div style={{ fontSize: "1rem", color: "#514F4F", marginTop: "30px" }}>
        <a href="/">click here</a> to go to home page
      </div>
    </div>
  );
}

export default NotFound;
