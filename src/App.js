import React from "react";
import "./App.css";
import Routes from "./routes/Routes";
import { withRouter } from "react-router-dom";
import { UserUpdateContext } from "./context/user";
import CompaniesUpdateProvider from "./context/companies";
import { useContext } from "react";

function App() {
  const { user } = useContext(UserUpdateContext);
  console.log(user);

  return (
    <React.Fragment>
      <CompaniesUpdateProvider>
        <Routes></Routes>
      </CompaniesUpdateProvider>
    </React.Fragment>
  );
}

export default withRouter(App);
